package nga.sps.rule.runner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.siliconmtn.io.http.SMTHttpConnectionManager;
import com.siliconmtn.io.sms.BulkSMSConfig;
import com.siliconmtn.io.sms.BulkSMSSender;
import com.siliconmtn.io.sms.SMSFactory;
import com.siliconmtn.openapi.OpenAPIBean;
import com.siliconmtn.openapi.OpenAPIConfig;
import com.siliconmtn.pulsar.PulsarClientManager;
import com.siliconmtn.pulsar.PulsarConfig;

import nga.oe.config.ApplicationConfig;
import nga.oe.connection.RestConnection;
import nga.oe.integrations.RPEConfig;
import nga.oe.integrations.RulesAndPolicyReleaseClient;
import nga.oe.pulsar.MessageSender;
import nga.oe.pulsar.RequestDTOMessageListener;
import nga.oe.schema.SchemaUtil;
import nga.oe.schema.aspect.LoggerAoP;
import nga.oe.security.request.RequestMetaDataBean;
import nga.oe.security.request.RequestMetaDataFilter;
import nga.sps.rule.runner.controllers.AsyncController;

/****************************************************************************
 * <b>Title:</b> Application.java <br>
 * <b>Project:</b> session-admin <br>
 * <b>Description:</b> Spring entrypoint <br>
 * <b>Copyright:</b> Copyright (c) 2023 <br>
 * <b>Company:</b> Silicon Mountain Technologies
 *
 * @author Joe Vahlenkamp
 * @version 3.x
 * @since June 14, 2023
 *        <b>updates:</b>
 *
 ****************************************************************************/
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class,
        HibernateJpaAutoConfiguration.class})
@Import({ApplicationConfig.class, OpenAPIConfig.class, OpenAPIBean.class,
        PulsarConfig.class, PulsarClientManager.class, RequestDTOMessageListener.class,
        MessageSender.class, SchemaUtil.class, SMSFactory.class, BulkSMSSender.class,
        BulkSMSConfig.class, SMTHttpConnectionManager.class, LoggerAoP.class,
        RulesAndPolicyReleaseClient.class, RPEConfig.class, RestConnection.class,
        RequestMetaDataBean.class, RequestMetaDataFilter.class})
@EnableScheduling
public class Application {

    @Autowired
    AsyncController asyncController;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
