package nga.sps.rule.runner.controllers;

import com.siliconmtn.io.api.base.BaseDTO;
import com.siliconmtn.pulsar.PulsarConfig;
import com.siliconmtn.pulsar.TopicConfig;
import lombok.extern.log4j.Log4j2;
import nga.oe.pulsar.RequestDTOMessageListener;
import nga.sps.rule.runner.services.AsyncRequestService;

import java.util.ArrayList;

import org.apache.pulsar.client.api.Consumer;
import org.apache.pulsar.client.api.ConsumerBuilder;
import org.apache.pulsar.client.api.PulsarClient;
import org.apache.pulsar.client.api.PulsarClientException;
import org.apache.pulsar.client.api.SubscriptionType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/****************************************************************************
 * <b>Title:</b> AsyncController.java <br>
 * <b>Project:</b> session-admin <br>
 * <b>Description:</b> Controller for managing lifecycle and ingestion of
 * inbound messaging data <br>
 * <b>Copyright:</b> Copyright (c) 2023 <br>
 * <b>Company:</b> Silicon Mountain Technologies
 *
 * @author Joe Vahlenkamp
 * @version 3.x
 * @since June 14, 2023
 *        <b>updates:</b>
 *
 ****************************************************************************/
@Component
@Configuration
@Log4j2
public class AsyncController {

    public static final String TOPIC = "rtpPayload";

    @Value("${conf.microServiceId}")
    String subscriptionName;

    @Autowired
    PulsarClient client;

    @Autowired
    PulsarConfig config;

    @Autowired
    RequestDTOMessageListener<AsyncRequestService<BaseDTO>> listener;

    /**
     * Manages connecting to and listening to the inbound topic
     *
     * @return the instance of the consumer with the attached listener
     */
    @Bean
    public Consumer<byte[]> asyncListener() {
        try {
            TopicConfig topicConfig = config.getTopics().get(TOPIC);
            final ConsumerBuilder<byte[]> consumerBuilder = client.newConsumer()
                    .consumerName(topicConfig.getName())
                    .subscriptionType(SubscriptionType.Shared)
                    .subscriptionName(subscriptionName)
                    .topic(topicConfig.getTopicUri()).messageListener(listener);

            return consumerBuilder.subscribe();
        } catch (PulsarClientException e) {
            log.error("Could not connect to Queue", e);
        }

        return null;
    }
}
