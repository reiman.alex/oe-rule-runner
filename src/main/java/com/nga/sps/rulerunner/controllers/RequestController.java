package nga.sps.rule.runner.controllers;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.siliconmtn.io.api.EndpointResponse;

import io.swagger.v3.oas.annotations.Operation;
import nga.oe.schema.aspect.SendAroundLogs;
import nga.oe.schema.exception.OEException;
import nga.oe.schema.vo.sps.GenericDTO;
import nga.oe.schema.vo.sps.RuleRunnerDTO;
import nga.sps.rule.runner.services.EndpointService;
import nga.sps.rule.runner.services.RuleService;

/****************************************************************************
 * <b>Title:</b> RequestController.java <br>
 * <b>Project:</b> session-admin <br>
 * <b>Description:</b> Manages processing RESTful requests to the service <br>
 * <b>Copyright:</b> Copyright (c) 2023 <br>
 * <b>Company:</b> Silicon Mountain Technologies
 *
 * @author Joe Vahlenkamp
 * @version 3.x
 * @since June 14, 2023
 *        <b>updates:</b>
 *
 ****************************************************************************/
@RestController
public class RequestController {

    public static final String SPS = "Rule Runner ";

    @Value("${pulsar.env}")
    public String env;

    @Autowired
    RuleService service;

    @Autowired
    EndpointService endpointService;

    /**
     * Handle heart beat liveliness checks
     *
     * @return Name of service
     */
    @Operation(summary = "Heartbeat", description = "Heartbeat for liveliness Check. Returns the Server Name.", tags = {"Service Profile Service"})
    @GetMapping("/")
    public EndpointResponse heartbeat() {
        return new EndpointResponse(SPS + env);
    }

    /**
     * Processes the data from the Rule Runner DTO
     *
     * @param ruleDTO
     * @return Data of the Rule Runner DTO
     * @deprecated
     */
    @SendAroundLogs
    @Deprecated(since = "January 30, 2024")
    @Operation(summary = "Process Rules", description = "Runs the data through RPE rules, providing any additional post processing necessary.", tags = {"Rules"})
    @PostMapping("/process")
    public EndpointResponse processData(@RequestBody RuleRunnerDTO ruleDTO) {
    	return new EndpointResponse(ruleDTO.getData());
    }

    /**
     * Processes the data from the Rule Runner DTO through RPE's rule engine.
     *
     * @param ruleDTO
     * @return
     * @throws OEException
     */
    @SendAroundLogs
    @Operation(summary = "Process Rules", description = "Runs the data through RPE rules, providing any additional post processing necessary.", tags = {"Rules"})
    @PostMapping("/evaluate")
    public EndpointResponse processRules(@RequestBody RuleRunnerDTO ruleDTO) throws OEException {
    	return new EndpointResponse(service.startRuleProcessing(ruleDTO, new HashMap<>()));
    }

    /**
     * Proxies retrieval of async documentation from the file manager since RPE
     * rules processing can't yet handle streaming file data.
     *
     * @param genericDTO
     * @return
     * @throws OEException
     */
    @SendAroundLogs
    @Operation(summary = "Retrieve Json File from File Manager", description = "Streams json data from the file manager and returns in a standard response.", tags = {"Rules"})
    @PostMapping("/retrieveJsonFile")
    public Map<String, Object> retrieveJsonFile(@RequestBody GenericDTO genericDTO) {
    	return endpointService.retrieveJsonFile((String) genericDTO.getData().get("filePath"));
    }
}
