package nga.sps.rule.runner.services;

import nga.oe.pulsar.MessageSender;
import nga.oe.schema.aspect.SendAroundLogs;
import nga.oe.schema.exception.OEException;
import nga.oe.schema.vo.RequestDTO;
import nga.oe.schema.vo.sps.AssetDTO;
import nga.oe.schema.vo.sps.RuleRunnerDTO;
import nga.oe.service.RequestServiceImpl;

import org.apache.pulsar.client.api.MessageId;
import org.apache.pulsar.client.api.Producer;
import org.apache.pulsar.client.api.PulsarClientException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.siliconmtn.pulsar.TopicConfig;

import lombok.Getter;
import lombok.extern.log4j.Log4j2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/****************************************************************************
 * <b>Title:</b> MessageService.java <br>
 * <b>Project:</b> session-admin <br>
 * <b>Description:</b> Manages business logic related to asynchronous requests <br>
 * <b>Copyright:</b> Copyright (c) 2023 <br>
 * <b>Company:</b> Silicon Mountain Technologies
 *
 * @author Cole Stephenson
 * @version 3.x
 * @since June 14, 2023
 *        <b>updates:</b>
 *        January 29, 2024 - Migrated Existing Code Out of Update-Router
 *
 ****************************************************************************/
@Service
@Log4j2
public class AsyncRequestService<T> implements RequestServiceImpl<T> {

	static final String SERVICE = "service";
	static final String PRODUCER_NAME = "sps-rule-runner";

	@Value("${conf.serviceId}")
	@Getter
	private String serviceEnv;

	@Value("${pulsar.env}")
	@Getter
	private String pulsarEnv;

	@Autowired
	MessageSender sender;

	@Autowired
	ObjectMapper mapper;

	@Autowired
	RuleService ruleService;

    /**
     * The Hash map type reference.
     */
    TypeReference<HashMap<String, Object>> hashMapTypeReference = new TypeReference<>() {};

    /*
     * (non-javadoc)
     *
     * @see
     * nga.oe.service.RequestServiceImpl#processRequest(nga.oe.schema.vo.RequestDTO)
     */
    @Override
    @SendAroundLogs
    public List<T> processRequest(RequestDTO req) throws OEException {
    	// Check if the request is for the configured service.
    	if (isForService(req)) {
    		// Log the data of the DTO
    		log.info("\n*******\nDTO Data: {}", req.getData());
    		log.info("\n*******\nDTO Schema: {}", req.getSchema());
    		log.info("\n*******\nDTO Properties: {}\n*******", req.getProperties());

    		// Evaluate rules against the incoming data.
    		AssetDTO evaluation = processRules(req);
    		try {
				req.setData(mapper.writeValueAsString(evaluation));
				req.setProperties(evaluation.getProperties());
			} catch (JsonProcessingException e) {
				throw new OEException(e.getMessage());
			}

    		// Forward the message to the configured topic(s).
    		forwardToTopics(req, evaluation.getTopics());
    	}

        return new ArrayList<>();
    }

	/**
	 * Checks if the request is for the configured service.
	 *
	 * @param req
	 * @return A boolean to determine if the RequestDTO coming in is intended for configured service through the schema's service property
	 */

    public boolean isForService(RequestDTO req) {
    	return req.hasProperty(SERVICE) && req.getProperty(SERVICE).equals(getServiceEnv());
    }

    /**
     * Process rules against the incoming data through RPE.
     *
     * @param req
     * @return
     * @throws OEException
     */
    private AssetDTO processRules(RequestDTO req) throws OEException {
		try {
			JsonNode evaluationNode = mapper.readTree(req.getData());

			RuleRunnerDTO ruleDTO = new RuleRunnerDTO();
	    	ruleDTO.setData(mapper.convertValue(evaluationNode, hashMapTypeReference));

	    	return ruleService.startRuleProcessing(ruleDTO, req.getProperties());
		} catch (JsonProcessingException e) {
			throw new OEException(e.getMessage());
		}
    }

    /**
     * Forwards the message to the configured list of topics.
     *
     * @param req
     * @param topics
     * @return
     */
    public void forwardToTopics(RequestDTO req, List<String> topics) {
		try {
			for (String topic : topics) {
		    	// Log the downstream topic
		    	log.info("Downstream Topic: {}", topic);

		    	// Send the message based on the pulsar environment.
		    	sendMessage(req, topic.replace("%env%", getPulsarEnv()));
			}
		} catch(PulsarClientException e) {
			log.error("Message Failed to Send", e);
		}
    }

    /**
     * Forwards a message to the configured topic.
     *
     * @param dto
     * @param topic
     * @return The MessageId of the new Pulsar message after it has been sent out to the appropriate topic.
     * @throws PulsarClientException
     */
    public MessageId sendMessage(RequestDTO dto, String topic) throws PulsarClientException {
    	// Log the DTO before it gets repackaged.
    	log.info("\n*******\nFinal Data: {}", dto.getData());
    	log.info("\n*******\nFinal Schema: {}", dto.getSchema());
    	log.info("\n*******\nFinal Properties: {}\n*******", dto.getProperties());

    	// Create a new TopicConfig.
    	TopicConfig tConfig = new TopicConfig();

    	// Manually set topic and name
    	tConfig.setTopicUri(topic);
    	tConfig.setName(PRODUCER_NAME);

    	// Initialize the MessageID.
		MessageId mId = null;

		try(Producer<byte[]> p = sender.buildProducer(tConfig)) {
			// Package the schema and data into a new DTO.
			RequestDTO rdto = new RequestDTO(dto.getSchema(), dto.getData());

			// Log the DTO before it gets sent back out.
			log.info(rdto);

			// Send the message.
			mId = p.newMessage()
				.value(mapper.writeValueAsBytes(rdto))
				.properties(dto.getProperties())
				.send();

		} catch (JsonProcessingException e) {
			log.error("Error", e);
		}
		return mId;
    }
}
