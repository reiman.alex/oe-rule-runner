package nga.sps.rule.runner.services;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.siliconmtn.data.text.StringUtil;

import lombok.extern.log4j.Log4j2;
import nga.oe.config.EndpointConfig;
import nga.oe.connection.RestConnection;
import nga.oe.schema.exception.AppSchemaException;

/****************************************************************************
 * <b>Title:</b> EndpointService.java
 * <b>Project:</b> Rule-Runner
 * <b>Description:</b> Services to confirm success of endpoints.
 * <b>Copyright:</b> Copyright (c) 2024
 * <b>Company:</b> Silicon Mountain Technologies
 *
 * @author Alex Reiman
 * @version 3.x
 * @since Aug 4, 2023
 * <b>updates:</b>
 *
 ****************************************************************************/

@Log4j2
@Service
public class EndpointService {
    /**
     * The Connection manager.
     */
    @Autowired
	RestConnection connect;

    /**
     * The Mapper.
     */
    @Autowired
    ObjectMapper mapper;

    /**
     * Environmental variable for the kong domain.
     */
    @Value("${kong.env.domain}")
    String kongDomain;

    /**
     * Environmental variable for the kong scheme.
     */
    @Value("${kong.env.scheme}")
    String kongScheme;

    /**
     * Environmental variable for the endpoint path to retrieve a file from the file manager.
     */
    @Value("${fileManager.retrieveFile.path}")
    String fileManagerKongPath;

    /**
     * The Hash map type reference.
     */
    TypeReference<HashMap<String, Object>> hashMapTypeReference = new TypeReference<>() {};

	/**
	 * Streams json data from the file manager and returns as a hash map.
	 *
	 * @param filePath
	 * @return
	 */
	public Map<String, Object> retrieveJsonFile(String filePath) {
		filePath = filePath.trim();

		if (StringUtil.isEmpty(filePath))
			return new HashMap<>();

		// Build a new EndpointConfig for the file manager.
		EndpointConfig fileManagerAPI = buildEndpoint(fileManagerKongPath, filePath);
		log.info("Retrieving JSON data at: {}", fileManagerAPI);

		try {
			// Process the response.
			JsonNode response = connect.processWithEndpointStream(fileManagerAPI, "", JsonNode.class, HttpMethod.GET);
			log.info("File Manager API Response: {}", response);

			// Return the json file data.
			return mapper.convertValue(response, hashMapTypeReference);
		} catch (AppSchemaException | IOException e) {
			log.error(e);
			return new HashMap<>();
		}
	}

    /**
     * Builds an EndpointConfig.
     *
     * @param pathRoot the root of the path
     * @param pathSuffix the ending of the specific path.
     * @return an EndpointConfig with the needed data.
     */
    public EndpointConfig buildEndpoint(String pathRoot, String pathSuffix) {
    	EndpointConfig endpoint = new EndpointConfig();
    	endpoint.setPath(pathRoot + pathSuffix);
    	endpoint.setDomain(kongDomain);
    	endpoint.setScheme(kongScheme);

    	return endpoint;
    }
}
