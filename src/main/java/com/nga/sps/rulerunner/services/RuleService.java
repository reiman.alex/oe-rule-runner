package nga.sps.rule.runner.services;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.siliconmtn.io.http.SMTHttpConnectionManager;

import lombok.extern.log4j.Log4j2;
import nga.oe.integrations.OETrustCerts;
import nga.oe.integrations.RPEConfig;
import nga.oe.integrations.RPEEvaluationDTO;
import nga.oe.integrations.RPEEvaluationDTO.RPEReleaseType;
import nga.oe.integrations.RPEEvaluationResponseDTO;
import nga.oe.integrations.RulesAndPolicyReleaseClient;
import nga.oe.schema.aspect.SendAroundLogs;
import nga.oe.schema.exception.OEException;
import nga.oe.schema.vo.sps.AssetDTO;
import nga.oe.schema.vo.sps.RuleRunnerDTO;
import nga.oe.security.request.RequestMetaDataBean;

/****************************************************************************
 * <b>Title:</b> RuleService.java
 * <b>Project:</b> Rule-Runner
 * <b>Description: Manages calls to RTP to process rules</b>
 * <b>Copyright:</b> Copyright (c) 2024
 * <b>Company:</b> Silicon Mountain Technologies
 *
 * @author Cole Stephenson
 * @version 3.x
 * @since January 24, 2024
 *        <b>updates:</b>
 *        January 24, 2024 - Cole Stephenson - Added Metrics Defaults
 *        January 30, 2024 - Tim Johnson - Added Core RPE Processing, Removed Metrics Defaults
 ****************************************************************************/
@Service
@Log4j2
public class RuleService {

	/**
     * The Base rule id.
     */
    @Value("${ruleRunner.baseRuleGroupId}")
    UUID baseRuleGroupId;

    /**
     * The RPE release group status
     */
    @Value("${ruleRunner.releaseGroupStatus}")
    String releaseGroupStatus;

    /**
     * The RPE release type
     */
    @Value("${ruleRunner.releaseType}")
    RPEReleaseType releaseType;

    /**
     * Environmental variable for the kong domain.
     */
    @Value("${kong.env.domain}")
    String kongDomain;

    /**
     * Environmental variable for the kong scheme.
     */
    @Value("${kong.env.scheme}")
    String kongScheme;

    /**
     * Environmental variable for the endpoint path to retrieve a file from the file manager.
     */
    @Value("${fileManager.retrieveFile.path}")
    String fileManagerKongPath;

    /**
     * Path to the rule runner.
     */
    @Value("${ruleRunner.route}")
    String ruleRunnerRoute;

    /**
     * Environment name
     */
    @Value("${pulsar.env}")
    public String env;

    /**
     * RPE Configuration
     */
    @Autowired
    RPEConfig rpeConfig;

    /**
     * The Object Mapper
     */
	@Autowired
	ObjectMapper mapper = new ObjectMapper();

    /**
     * The SMT Connection manager
     */
    @Autowired
    SMTHttpConnectionManager connManager = new SMTHttpConnectionManager(new OETrustCerts().getTrustCerts().getSocketFactory());

    /**
     * RPE Client
     */
    @Autowired
    RulesAndPolicyReleaseClient rpeClient = new RulesAndPolicyReleaseClient(connManager, mapper, rpeConfig);

    /**
     * Request meta data
     */
    @Autowired
    RequestMetaDataBean requestMetaData;

    /**
     * The Hash map type reference.
     */
    TypeReference<HashMap<String, Object>> hashMapTypeReference = new TypeReference<>() {};

    /**
     * Kicks off the rule processing for the supplied asset.
     *
     * @param rule
     * @return
     * @throws OEException
     */
    @SendAroundLogs
    public AssetDTO startRuleProcessing(RuleRunnerDTO rule, Map<String, String> properties) throws OEException {
    	setDefaultRuleGroupId(rule);

    	// Build the initial asset to run rules against.
    	AssetDTO firstAsset = new AssetDTO();
    	firstAsset.setAsset(rule.getData());

    	// Set required environmental values.
    	Map<String, Object> environment = new HashMap<>();
    	environment.put("kongScheme", kongScheme);
    	environment.put("kongDomain", kongDomain);
    	environment.put("fileManagerKongPath", fileManagerKongPath);
    	environment.put("ruleRunnerRoute", ruleRunnerRoute);
    	environment.put("env", env);
    	firstAsset.setEnvironment(environment);

    	// Set the initial properties.
    	firstAsset.setProperties(requestMetaData.updateProperties(properties));

    	// Set the asset back to the rule runner object for processing.
    	rule.setData(mapper.convertValue(firstAsset, hashMapTypeReference));

    	// Kick off the rule processing for the supplied asset.
    	return processRules(rule);
    }

    /**
     * Set's the default rule group id to run, if one doesn't already exist.
     *
     * @param rule
     */
    private void setDefaultRuleGroupId(RuleRunnerDTO rule) {
    	if (rule.getRuleGroupId() == null) {
    		rule.setRuleGroupId(baseRuleGroupId);
    	}
    }

    /**
     * Processes rules for the given asset.
     *
     * @param rule
     * @return
     * @throws OEException
     */
    private AssetDTO processRules(RuleRunnerDTO rule) throws OEException {
    	// Run the rule evaluation.
    	AssetDTO evaluation = evaluateRules(rule);

    	// Keep evaluating rules until there are none left to process.
    	if (!evaluation.getRuleGroupIds().isEmpty()) {
    		// Create a new object from the previous evaluation so we don't have to keep track of state.
    		RuleRunnerDTO nextRule = new RuleRunnerDTO();
    		nextRule.setRuleGroupId(evaluation.getRuleGroupIds().remove(0));
    		nextRule.setData(mapper.convertValue(evaluation, hashMapTypeReference));
    		return processRules(nextRule);
    	} else {
    		return evaluation;
    	}
    }

    /**
     * Evaluate asset rules against RPE.
     *
     * @param rule
     * @return
     * @throws OEException
     */
    private AssetDTO evaluateRules(RuleRunnerDTO rule) throws OEException {
    	// Set the evaluation object.
    	RPEEvaluationDTO eval = new RPEEvaluationDTO();
		eval.setReleaseType(releaseType);
		eval.setReleaseGroupId(rule.getRuleGroupId());
		eval.setReleaseGroupStatus(releaseGroupStatus);
		log.info("Evaluating Rule Release: {} | {} | {}", eval.getReleaseType(), eval.getReleaseGroupId(), eval.getReleaseGroupStatus());

		// Set the context on the evaluation object.
		JsonNode evaluationNode = mapper.valueToTree(rule.getData());
		eval.setContext(mapper.convertValue(evaluationNode, hashMapTypeReference));
		log.info("Original Context: {}", eval.getContext());

		// Get the evaluation response.
		RPEEvaluationResponseDTO response = rpeClient.evaluate(eval);
    	JsonNode contextNode = mapper.valueToTree(response.getContext());
		log.info("Updated Context: {}", response.getContext());

		// Create the Asset
		AssetDTO asset = mapper.convertValue(contextNode, AssetDTO.class);
		asset.setValid(asset.isValid() && response.isResultState());

		// Return the asset.
    	return asset;
    }
}
