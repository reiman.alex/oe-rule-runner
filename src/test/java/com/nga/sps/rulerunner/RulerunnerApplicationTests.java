package nga.sps.rule.runner;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mockStatic;

/****************************************************************************
 * <b>Title:</b> ApplicationTest.java <br>
 * <b>Project:</b> session-admin <br>
 * <b>Description:</b> CHANGE ME!! <br>
 * <b>Copyright:</b> Copyright (c) 2022 <br>
 * <b>Company:</b> Silicon Mountain Technologies
 *
 * @author Joe Vahlenkamp
 * @version 3.x
 * @since Nov 15, 2022
 *        <b>updates:</b>
 *
 ****************************************************************************/

class ApplicationTest {


    @BeforeEach
    protected void setUp() throws Exception {
    }

    @Test
    void testMainTest() throws Exception {
        try (MockedStatic<SpringApplication> mocked = mockStatic(
                SpringApplication.class)) {

            mocked.when(() -> {
                SpringApplication.run(Application.class, "foo", "bar");
            }).thenReturn(Mockito.mock(ConfigurableApplicationContext.class));

            Application.main(new String[]{"foo", "bar"});

            mocked.verify(() -> {
                SpringApplication.run(Application.class, "foo", "bar");
            });

        }
    }


    @Test
    void testMainConstructor() throws Exception {
        assertNotNull(new Application());
    }

}
