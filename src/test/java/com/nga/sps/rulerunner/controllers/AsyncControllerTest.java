package nga.sps.rule.runner.controllers;

import com.siliconmtn.io.api.base.BaseDTO;
import com.siliconmtn.pulsar.PulsarConfig;
import com.siliconmtn.pulsar.TopicConfig;
import nga.oe.pulsar.RequestDTOMessageListener;
import nga.sps.rule.runner.services.AsyncRequestService;
import org.apache.pulsar.client.api.Consumer;
import org.apache.pulsar.client.api.ConsumerBuilder;
import org.apache.pulsar.client.api.PulsarClient;
import org.apache.pulsar.client.api.PulsarClientException;
import org.apache.pulsar.client.api.SubscriptionType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ActiveProfiles;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.mock;

/****************************************************************************
 * <b>Title:</b> AsyncControllerTest.java <br>
 * <b>Project:</b> session-admin <br>
 * <b>Description:</b> CHANGE ME!! <br>
 * <b>Copyright:</b> Copyright (c) 2022 <br>
 * <b>Company:</b> Silicon Mountain Technologies
 *
 * @author Joe Vahlenkamp
 * @version 3.x
 * @since Nov 14, 2022
 *        <b>updates:</b>
 *
 ****************************************************************************/
@ActiveProfiles("test")
class AsyncControllerTest {

    @Mock
    RequestDTOMessageListener<AsyncRequestService<BaseDTO>> listener;

    @Mock
    PulsarClient pulsarClient;

    @Mock
    ConsumerBuilder<byte[]> consumerBuilder;

    @Mock
    TopicConfig tConfig;

    @Mock
    PulsarConfig config;

    @Mock
    Consumer<byte[]> consumer;

    @InjectMocks
    AsyncController controller;


    @BeforeEach
    protected void setUp() throws Exception {
        tConfig = mock(TopicConfig.class);
        Map<String, TopicConfig> topics = new HashMap<>();
        topics.put(AsyncController.TOPIC, tConfig);
        MockitoAnnotations.openMocks(this);
        Mockito.when(pulsarClient.newConsumer()).thenReturn(consumerBuilder);
        Mockito.when(config.getTopics()).thenReturn(topics);
        Mockito.when(consumerBuilder.consumerName(tConfig.getName()))
                .thenReturn(consumerBuilder);
        Mockito.when(consumerBuilder.topic(tConfig.getTopicUri()))
                .thenReturn(consumerBuilder);
        Mockito.when(
                        consumerBuilder.subscriptionName(tConfig.getUniqueSubscriptionName()))
                .thenReturn(consumerBuilder);
        Mockito.when(consumerBuilder.subscriptionType(SubscriptionType.Shared)).thenReturn(consumerBuilder);
        Mockito.when(consumerBuilder.messageListener(listener))
                .thenReturn(consumerBuilder);
    }

    @Test
    void apmListenerTest() throws Exception {
        Mockito.when(consumerBuilder.subscribe()).thenReturn(consumer);

        Assertions.assertDoesNotThrow(() -> controller.asyncListener());
    }

    @Test
    void apmListenerFailTest() throws Exception {
        Mockito.when(consumerBuilder.subscribe())
                .thenThrow(new PulsarClientException("Connection Refused"));

        Assertions.assertDoesNotThrow(() -> controller.asyncListener());
        assertNull(controller.asyncListener());
    }

}
