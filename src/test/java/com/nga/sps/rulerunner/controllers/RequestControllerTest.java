package nga.sps.rule.runner.controllers;

import java.io.IOException;
import java.util.HashMap;

import com.siliconmtn.io.api.EndpointResponse;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import nga.oe.schema.vo.sps.RuleRunnerDTO;
import static org.junit.jupiter.api.Assertions.assertEquals;

/****************************************************************************
 * <b>Title:</b> RequestControllerTest.java <br>
 * <b>Project:</b> session-admin <br>
 * <b>Description:</b> CHANGE ME!! <br>
 * <b>Copyright:</b> Copyright (c) 2022 <br>
 * <b>Company:</b> Silicon Mountain Technologies
 *
 * @author Joe Vahlenkamp
 * @version 3.x
 * @since Nov 14, 2022
 *        <b>updates:</b>
 *
 ****************************************************************************/
@ExtendWith(SpringExtension.class)
class RequestControllerTest {


    @InjectMocks
    private RequestController requestController;

    /**
     * Tests the heartbeat of the endpoint.
     */
    @Test
    void testHeartbeat() {
        EndpointResponse actualRes = requestController.heartbeat();
        EndpointResponse responseEntity = new EndpointResponse(HttpStatus.OK);
        responseEntity.setTimestamp(actualRes.getTimestamp());
        responseEntity.setSuccess(true);
        responseEntity.setData(RequestController.SPS + requestController.env);
        assertEquals(responseEntity, actualRes);
    }

    /**
     * Tests whether the data processed by the method is correct.
     * @throws IOException
     */
    @Test
    void testProcessData() throws IOException {
    	// Build a map of the data from the JSON strings that will be passed in.
    	ObjectMapper mapper = new ObjectMapper();
    	String testData = "{\"name\": \"Test\", \"description\": \"Testing echo\"}";
    	String testRuleId = "Test Rule Id";
    	TypeReference<HashMap<String, Object>> typeRef
		  = new TypeReference<HashMap<String, Object>>() {};

		// Assign the data map to a test DTO
		RuleRunnerDTO testDTO = new RuleRunnerDTO();
		HashMap<String, Object> map = mapper.readValue(testData, typeRef);
		testDTO.setData(map);
		testDTO.setRuleId(testRuleId);

		// Pass test DTO into the method and compare its results to a map of the data.
		EndpointResponse actualRes = requestController.processData(testDTO);
		HashMap<String, Object> map2 = mapper.readValue(testData, typeRef);

		assertEquals(map2, actualRes.getData());
    }
}
