package nga.sps.rule.runner.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.pulsar.client.api.MessageId;
import org.apache.pulsar.client.api.Producer;
import org.apache.pulsar.client.api.PulsarClientException;
import org.apache.pulsar.client.api.TypedMessageBuilder;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.siliconmtn.io.api.base.BaseDTO;
import com.siliconmtn.pulsar.TopicConfig;

import nga.oe.pulsar.MessageSender;
import nga.oe.schema.exception.OEException;
import nga.oe.schema.vo.RequestDTO;
import nga.oe.schema.vo.sps.AssetDTO;

/****************************************************************************
 * <b>Title:</b> AsyncRequestServiceTest.java <br>
 * <b>Project:</b> rule-runner <br>
 * <b>Description:</b> CHANGE ME!! <br>
 * <b>Copyright:</b> Copyright (c) 2022 <br>
 * <b>Company:</b> Silicon Mountain Technologies
 *
 * @author Cole Stephenson
 * @version 3.x
 * @since July 11, 2023
 *        <b>updates:</b>
 *        January 29, 2024 - Migrated Existing Code Out of Update-Router
 *
 ****************************************************************************/
@ExtendWith(MockitoExtension.class)
class AsyncRequestServiceTest {

	public final String APM = "APM";
	public final String SPS = "SPS";

	@Mock
    RequestDTO dto = new RequestDTO();

    @Mock
    ObjectMapper mapper;

    @Mock
    MessageSender sender;

    @Mock
    Producer<byte[]> producer;

    @Mock
    TypedMessageBuilder<byte[]> typedMessageBuilder;

    @Mock
    MessageId mId;

    @InjectMocks
    @Spy
    AsyncRequestService<BaseDTO> service;

    @Spy
    RuleService rService;

    TypeReference<HashMap<String, Object>> hashMapTypeReference = new TypeReference<>() {};

    /**
     * Tests to validate the execution of processRequest when isForService is true
     * @throws PulsarClientException
     * @throws OEException
     */
    @Test
    void processRequestisForServiceTrue() throws PulsarClientException, OEException {
    	AssetDTO adto = new AssetDTO();
    	Mockito.doReturn(true).when(service).isForService(dto);
    	Mockito.doReturn(adto).when(rService).startRuleProcessing(any(), any());
    	service.processRequest(dto);
    	Mockito.verify(service).isForService(dto);
    }

    /**
     * Tests to validate that the internal methods of processRequest do not execute when isForService is false
     * @throws PulsarClientException
     * @throws OEException
     */
    @Test
    void processRequestisForServiceFalse() throws PulsarClientException, OEException {
    	Mockito.doReturn(false).when(service).isForService(dto);
    	List<?> result = service.processRequest(dto);
    	Mockito.verify(service).isForService(dto);
    	assertEquals(result, new ArrayList<>());

    }

    /**
     * Tests to validate that the correct exception is thrown when internal methods of processRequest fail a try/catch
     * @throws PulsarClientException
     * @throws OEException
     */
    @Test
    void processRequestThrowsException() throws JsonProcessingException, OEException {
    	AssetDTO adto = new AssetDTO();
    	Mockito.doReturn(true).when(service).isForService(dto);
    	Mockito.doReturn(adto).when(rService).startRuleProcessing(any(), any());
    	Mockito.doThrow(new JsonProcessingException("error") {}).when(mapper).writeValueAsString(any(AssetDTO.class));
    	assertThrows(OEException.class, () -> service.processRequest(dto));

    }


    /**
     * Tests to validate that the correct exception is thrown when internal methods of processRequest fail a try/catch
     * @throws PulsarClientException
     * @throws OEException
     */
    @Test
    void processRulesThrowsException() throws JsonProcessingException, OEException {
    	RequestDTO rdto = new RequestDTO();
    	rdto.setData("{\"microservice\":{\"chargebackCode\":\"CB Code\",\"description\":\"Handles microservice and service core data, providing data validation before saving. Ensures heartbeat is live, and retrieves Swagger data. \",\"heartbeatPath\":\"/\",\"introspectiveAPIPath\":\"/v3/api-docs\",\"nonPersistentAPIFilePath\":\"/Home/OE_Teams/Service_Profile_Service/swagger.json\",\"jarmID\":\"5.03.02.02\",\"kongPath\":\"/oe/service-profile-service/microservice-manager/api/1\",\"metadata\":{\"activeFlag\":true,\"schemaId\":\"118597b3-6041-4579-ad84-9ef71d5ab226\",\"createDate\":\"2023-09-22T01:31:06.860+00:00\",\"updateDate\":\"2024-02-07T16:14:41.339+00:00\",\"versionGroupId\":\"d7e91971-49e7-4fe5-bf27-c08bfb5e1c83\",\"versionOrderNumber\":17,\"current\":true},\"metricIdentifierCode\":\"sps:microservice-manager\",\"name\":\"Microservice Manager\",\"serviceCode\":\"SPS\",\"serviceLayer\":\"presentation\",\"serviceType\":\"persistent\",\"tags\":[\"microservice\"],\"versionNumberText\":\"0.0.2\"}}");
    	Mockito.doReturn(true).when(service).isForService(rdto);
    	Mockito.doThrow(new JsonProcessingException("error") {}).when(mapper).readTree(any(String.class));
    	assertThrows(OEException.class, () -> service.processRequest(rdto));

    }

    /**
     * Validates that isForService returns true if the DTO has the correct properties to execute
     */
    @Test
    void isMessageForServiceTestTrue() {
    	Mockito.when(service.getServiceEnv()).thenReturn(SPS);
    	Mockito.when(dto.hasProperty(AsyncRequestService.SERVICE)).thenReturn(true);
    	Mockito.when(dto.getProperty(AsyncRequestService.SERVICE)).thenReturn(SPS);
    	assertTrue(service.isForService(dto));
    }

    /**
     * Validates that isForService returns false when the DTO does not have a property called service
     */
    @Test
    void isMessageForServiceTestFalseNoServiceProperty() {
    	Mockito.when(dto.hasProperty(AsyncRequestService.SERVICE)).thenReturn(false);
    	assertFalse(service.isForService(dto));
    }

    /**
     * Validates that isForService returns false when the DTO service property returns something other than the configured service
     */
    @Test
    void isMessageForServiceTestFalseNotService() {
    	Mockito.when(service.getServiceEnv()).thenReturn(SPS);
    	Mockito.when(dto.hasProperty(AsyncRequestService.SERVICE)).thenReturn(true);
    	Mockito.when(dto.getProperty(AsyncRequestService.SERVICE)).thenReturn(APM);
    	assertFalse(service.isForService(dto));
    }


    /**
     * Tests to validate that sendMessage is called when invoking forwardToTopics
     * @throws PulsarClientException
     * @throws OEException
     */
    @Test
    void fordwardToTopicsCallsSendMessage() throws PulsarClientException{
    	List<String> topics = List.of("%env%");
    	Mockito.doReturn("dev").when(service).getPulsarEnv();
    	Mockito.doReturn(null).when(service).sendMessage(any(), any());
    	service.forwardToTopics(dto, topics);
    	Mockito.verify(service).sendMessage(dto, "dev");

    }

    /**
     * Validates that when sendMessage is called, all the child methods within are called with their appropriate returns
     * @throws PulsarClientException
     * @throws JsonProcessingException
     */
    @Test
    void sendMessageTest() throws PulsarClientException, JsonProcessingException{
    	Mockito.when(sender.buildProducer(any(TopicConfig.class))).thenReturn(producer);
    	Mockito.when(producer.newMessage()).thenReturn(typedMessageBuilder);
    	Mockito.when(typedMessageBuilder.value(any())).thenReturn(typedMessageBuilder);
    	Mockito.when(typedMessageBuilder.properties(any())).thenReturn(typedMessageBuilder);
    	Mockito.when(typedMessageBuilder.send()).thenReturn(mId);
    	service.sendMessage(dto, APM);
    	Mockito.verify(sender).buildProducer(any(TopicConfig.class));
    	Mockito.verify(typedMessageBuilder).value(any());
    	Mockito.verify(typedMessageBuilder).properties(any());
    	Mockito.verify(typedMessageBuilder).send();
    }

    /**
     * Validates that sendMessage throws the correct exception of PulsarClientException when send method fails
     * @throws PulsarClientException
     * @throws JsonProcessingException
     */
    @Test
	  void sendMessageThrowsPulsarCientExceptionTest() throws PulsarClientException, JsonProcessingException{
	  	Mockito.when(sender.buildProducer(any(TopicConfig.class))).thenReturn(producer);
	  	Mockito.when(producer.newMessage()).thenReturn(typedMessageBuilder);
	  	Mockito.when(typedMessageBuilder.value(any())).thenReturn(typedMessageBuilder);
	  	Mockito.when(typedMessageBuilder.properties(any())).thenReturn(typedMessageBuilder);
	  	Mockito.doThrow(new PulsarClientException("error")).when(typedMessageBuilder).send();
		assertThrows(PulsarClientException.class, () -> service.sendMessage(dto, "foo"));
    }
}
