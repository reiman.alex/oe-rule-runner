package nga.sps.rule.runner.services;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Value;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import nga.oe.config.EndpointConfig;
import nga.oe.connection.RestConnection;
import nga.oe.schema.exception.AppSchemaException;

/****************************************************************************
* <b>Title:</b> EndpointServiceTest.java <br>
* <b>Project:</b> rule-runner <br>
* <b>Description:</b> CHANGE ME!! <br>
* <b>Copyright:</b> Copyright (c) 2022 <br>
* <b>Company:</b> Silicon Mountain Technologies
*
* @author Cole Stephenson
* @version 3.x
* @since February 21, 2024
*        <b>updates:</b>
*
****************************************************************************/
@ExtendWith(MockitoExtension.class)
public class EndpointServiceTest {

	@Mock
	RestConnection connect;

	@Spy
	ObjectMapper mapper = new ObjectMapper();

	@Value("${fileManager.retrieveFile.path}")
    String fileManagerKongPath;

	@Mock
	TypeReference<HashMap<String, Object>> hashMapTypeReference = new TypeReference<>() {};

	@InjectMocks
	@Spy
	EndpointService service;

	@Test
	void retrieveJsonFileReturnsEmptyHashMapWithBadInput() {
		assertTrue(service.retrieveJsonFile("") instanceof HashMap);
	}

	@Test
	void retrieveJsonFileReturnsData() throws AppSchemaException, IOException {
		Mockito.doReturn(mapper.createObjectNode()).when(connect).processWithEndpointStream(any(), any(), any(), any());
		Mockito.doReturn(Map.of("test", "test")).when(mapper).convertValue(any(JsonNode.class), eq(hashMapTypeReference));
		Map<String, Object> data = service.retrieveJsonFile("/test");
		assertThat(data).isNotEmpty();
	}

	@Test
	void buildEndpointReturnsEndpointConfigObject() {
		assertTrue(service.buildEndpoint("foo", "bar") instanceof EndpointConfig);
	}
}
