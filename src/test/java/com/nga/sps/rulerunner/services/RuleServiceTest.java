package nga.sps.rule.runner.services;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;
import org.springframework.test.context.junit.jupiter.web.SpringJUnitWebConfig;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import nga.oe.integrations.RPEEvaluationDTO;
import nga.oe.integrations.RPEEvaluationResponseDTO;
import nga.oe.integrations.RulesAndPolicyReleaseClient;
import nga.oe.schema.exception.OEException;
import nga.oe.schema.vo.sps.AssetDTO;
import nga.oe.schema.vo.sps.RuleRunnerDTO;
import nga.oe.security.request.RequestMetaDataBean;
/****************************************************************************
 * <b>Title:</b> RuleServiceTest.java <br>
 * <b>Project:</b> rule-runner <br>
 * <b>Description:</b> CHANGE ME!! <br>
 * <b>Copyright:</b> Copyright (c) 2022 <br>
 * <b>Company:</b> Silicon Mountain Technologies
 *
 * @author Cole Stephenson
 * @version 3.x
 * @since February 21, 2024
 *        <b>updates:</b>
 *
 ****************************************************************************/
@ExtendWith(MockitoExtension.class)
@SpringJUnitWebConfig
class RuleServiceTest {

    @Mock
    RuleRunnerDTO dto;

    @Spy
	ObjectMapper mapper = new ObjectMapper();

    @Mock
    RequestMetaDataBean requestMetaData;

    @InjectMocks
    @Spy
    RuleService service;

    @Mock
    RulesAndPolicyReleaseClient rpeClient;

	String json = "{\"microservice\":{\"chargebackCode\":\"CB Code\",\"description\":\"Handles microservice and service core data, providing data validation before saving. Ensures heartbeat is live, and retrieves Swagger data. \",\"heartbeatPath\":\"/\",\"introspectiveAPIPath\":\"/v3/api-docs\",\"nonPersistentAPIFilePath\":\"/Home/OE_Teams/Service_Profile_Service/swagger.json\",\"jarmID\":\"5.03.02.02\",\"kongPath\":\"/oe/service-profile-service/microservice-manager/api/1\",\"metadata\":{\"activeFlag\":true,\"schemaId\":\"118597b3-6041-4579-ad84-9ef71d5ab226\",\"createDate\":\"2023-09-22T01:31:06.860+00:00\",\"updateDate\":\"2024-02-07T16:14:41.339+00:00\",\"versionGroupId\":\"d7e91971-49e7-4fe5-bf27-c08bfb5e1c83\",\"versionOrderNumber\":17,\"current\":true},\"metricIdentifierCode\":\"sps:microservice-manager\",\"name\":\"Microservice Manager\",\"serviceCode\":\"SPS\",\"serviceLayer\":\"presentation\",\"serviceType\":\"persistent\",\"tags\":[\"microservice\"],\"versionNumberText\":\"0.0.2\"}}";


    @Test
    void startRuleProcessingReturnsAssetDTO() throws JsonMappingException, JsonProcessingException, OEException {
    	Map<String, String> properties = new HashMap<>();
    	RuleRunnerDTO rdto = new RuleRunnerDTO();
    	rdto.setRuleGroupId(UUID.randomUUID());
    	rdto.setData(mapper.readValue(json, new TypeReference<Map<String, Object>>() {}));
    	Mockito.when(rpeClient.evaluate(any(RPEEvaluationDTO.class))).thenAnswer(
    			new Answer() {

    				private boolean hasAnswered = false;

    				public RPEEvaluationResponseDTO answer(InvocationOnMock invocation) {
    					RPEEvaluationResponseDTO answerDTO = new RPEEvaluationResponseDTO();
    					RPEEvaluationDTO arg = (RPEEvaluationDTO) invocation.getArguments()[0];
    					AssetDTO context = mapper.convertValue(arg.getContext(), new TypeReference<AssetDTO>() {}) ;
    					if(!hasAnswered) {
    						hasAnswered = true;
    						context.getRuleGroupIds().add(UUID.randomUUID());
    					}
    					answerDTO.setContext(context);
    					return answerDTO;
    				}
    			}
    		);
    	assertTrue(service.startRuleProcessing(rdto, properties) instanceof AssetDTO);
    }

    @Test
    void startRuleProcessingReturnsAssetDTODefaultIdInjected() throws JsonMappingException, JsonProcessingException, OEException {
    	Map<String, String> properties = new HashMap<>();
    	RuleRunnerDTO rdto = new RuleRunnerDTO();
    	rdto.setData(mapper.readValue(json, new TypeReference<Map<String, Object>>() {}));
    	Mockito.when(rpeClient.evaluate(any(RPEEvaluationDTO.class))).thenAnswer(
    			new Answer() {

    				private boolean hasAnswered = false;

    				public RPEEvaluationResponseDTO answer(InvocationOnMock invocation) {
    					RPEEvaluationResponseDTO answerDTO = new RPEEvaluationResponseDTO();
    					RPEEvaluationDTO arg = (RPEEvaluationDTO) invocation.getArguments()[0];
    					AssetDTO context = mapper.convertValue(arg.getContext(), new TypeReference<AssetDTO>() {}) ;
    					if(!hasAnswered) {
    						hasAnswered = true;
    						context.getRuleGroupIds().add(UUID.randomUUID());
    					}
    					answerDTO.setContext(context);
    					return answerDTO;
    				}
    			}
    		);
    	assertTrue(service.startRuleProcessing(rdto, properties) instanceof AssetDTO);
    }
}
